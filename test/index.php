<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Тестовая страничка");

	ini_set('display_errors','On');
	header('Content-Type: text/plain;');
	error_reporting(E_ALL ^ E_WARNING);
	set_time_limit(0);
	ob_implicit_flush();
	echo "-= Server =-\n\n";
	$address = 'localhost';
	//$address = '127.0.0.1';
	$port = 10001;
	try {
		echo 'Create socket ... ';
		if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) < 0) {
			throw new Exception('socket_create() failed: '.socket_strerror(socket_last_error())."\n");
		} else {
			echo "OK\n";
		}
		echo 'Bind socket ... ';
		if (($ret = socket_bind($sock, $address, $port)) < 0) {
			throw new Exception('socket_bind() failed: '.socket_strerror(socket_last_error())."\n");
		} else {
			echo "OK\n";
		}
		echo 'Listen socket ... ';
		if (($ret = socket_listen($sock, 5)) < 0) {
			throw new Exception('socket_listen() failed: '.socket_strerror(socket_last_error())."\n");
		} else {
			echo "OK\n";
		} do {
			echo 'Accept socket ... ';
			if (($msgsock = socket_accept($sock)) < 0) {
				throw new Exception('socket_accept() failed: '.socket_strerror(socket_last_error())."\n");
			} else {
				echo "OK\n";
			}
			$msg = "Hello, Client!";
			echo "Say to client ($msg) ... ";
			socket_write($msgsock, $msg, strlen($msg));
			echo "OK\n";
			do {
				echo 'Client said: ';
				if (false === ($buf = socket_read($msgsock, 1024))) {
					throw new Exception('socket_read() failed: '.socket_strerror(socket_last_error())."\n");
				} else {
					echo $buf."\n";
				}
				if (!$buf = trim($buf)) {
					continue;
				}
				if ($buf == 'shutdown') {
					socket_close($msgsock);
					break 2;
				}
				echo "Say to client ($buf) ... ";
				socket_write($msgsock, $buf, strlen($buf));
				echo "OK\n";
				
			} while (true);
		} while (true);
	} catch (Exception $e) {
		echo "\nError: ".$e->getMessage();
	}
	if (isset($sock)) {
		echo 'Close socket ... ';
		socket_close($sock);
		echo "OK\n";
	}


 
 
/*
//Сначала нам нужно создать дескриптор сокета:
echo 'Create socket ... ';
if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) < 0) 
{
	throw new Exception('socket_create() failed: '.socket_strerror(socket_last_error())."\n");
} else {
	echo "OK\n"; 
}

//Теперь нам нужно привязать созданный дескриптор к определенным адресу и порту машины,
//на которой он будет запущен. Обычно локальный адрес – 127.0.0.1, или localhost. 
//Возьмем порт с номером 10001:
$address = 'localhost';
$port    = 10001;
echo 'Bind socket ... ';
if (($ret = socket_bind($sock, $address, $port)) < 0) 
{
	throw new Exception('socket_bind() failed: '.socket_strerror(socket_last_error())."\n");
} else {
	echo "OK\n";
}

//Затем нам нужно включить прослушивание этого сокета:
echo 'Listen socket ... ';
if (($ret = socket_listen($sock, 5)) < 0) {
	throw new Exception('socket_listen() failed: '.socket_strerror(socket_last_error())."\n"); 
} else {
	echo "OK\n"; 
}

//Когда клиент попытается установить с нами соединение, нам нужно его принять:
echo 'Accept socket ... ';
if (($msgsock = socket_accept($sock)) < 0) {
	throw new Exception('socket_accept() failed: '.socket_strerror(socket_last_error())."\n");
} else {
	echo "OK\n";
}


//Ну а дальше мы просто общаемся с клиентом, отправляя ему данные … 
$msg = "Hello, Client!";
echo "Say to client ($msg) ... ";
socket_write($msgsock, $msg, strlen($msg));
echo "OK\n";


//… или принимая их от него: 

echo 'Client said: ';
if (false === ($buf = socket_read($msgsock, 1024))) {
	throw new Exception('socket_read() failed: '.socket_strerror(socket_last_error())."\n");
} else {
	echo $buf."\n";
}

//После всего этого «разговора» двух программ, нужно освободить ресурсы, вызвав socket_close: 
echo 'Close socket ... ';
socket_close($sock);
echo "OK\n";
*/






//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>