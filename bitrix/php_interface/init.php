<?
function pre($array){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
	return;
}

# Подключаем пользовательские классы.
CModule::AddAutoloadClasses(
	'', // не указываем имя модуля
	array(
		// ключ - имя класса,
		// значение - путь относительно корня сайта к файлу с классом
		'HTMLSiteMapClass' => '/bitrix/php_interface/UserClass/HTMLSiteMapClass.php',
		'SiteMapClass' => '/bitrix/php_interface/UserClass/SiteMapClass.php',
		'tenderBD' => '/bitrix/php_interface/UserClass/tender/tenderBD.php',
		'callBack' => '/bitrix/php_interface/UserClass/tender/callback.php',
		'tenderAPI' => '/bitrix/php_interface/UserClass/tender/tenderAPI.php'
	)
);

AddEventHandler('main',   'OnEpilog',   '_Check404Error', 1);

function _Check404Error()
{
	if (defined('ERROR_404') && ERROR_404=='Y' && !defined('ADMIN_SECTION'))
	{
		GLOBAL $APPLICATION;
		$APPLICATION->RestartBuffer();
		include   $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/header.php';
		require   ($_SERVER['DOCUMENT_ROOT'].'/404.php');
		include   $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/footer.php';
	}
}