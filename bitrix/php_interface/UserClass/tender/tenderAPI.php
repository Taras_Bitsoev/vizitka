<?php
class tenderAPI{
	public  static $NOWTimeMinPrice = 0;
	private static $IBLOCKID = 16;
	private static $DB = false;
	private $ActiveTNDR;
	


	public  function getStartRate($IBLOCK_ELEMENT_ID=null)
	{
		if($IBLOCK_ELEMENT_ID===null) return false;
		$lotID = $this->getLotID($IBLOCK_ELEMENT_ID);
		return $startRate = $this->startRate($lotID);
	}
	public  function addRate()
	{
	
		return false;
	}
	public  function getBestRate($IBLOCK_ELEMENT_ID=null)
	{
		if($IBLOCK_ELEMENT_ID===null) return false;
		$lotID = $this->getLotID($IBLOCK_ELEMENT_ID);
		return $bestRate = $this->bestRate($lotID);
	}
	public  function refusedProve($bitrixElementID=null)
	{
		if($bitrixElementID===null) return false;
		$lotID = $this->getLotID($bitrixElementID);
		$UserInfo = $this->getUserInfo();
		$UserIdBD = $this->addPartner($UserInfo);
		return $actionID = $this->refusedAccount($lotID, $UserIdBD);
	}
	public  function addProve($bitrixElementID=null)
	{
		if($bitrixElementID===null) return false;
		$lotID = $this->getLotID($bitrixElementID);
		$UserInfo = $this->getUserInfo();
		$UserIdBD = $this->addPartner($UserInfo);
		return $actionID = $this->AddAccount($lotID, $UserIdBD);
	}
	public  function getUserInfo($userIdBitrix=null)
	{
		global $USER;
		if($userIdBitrix===null)
		{
			$UserID = $USER->GetID();
		} else {
			$UserID = $userIdBitrix;
		}
		
		$rsUser = CUser::GetByID($UserID);
		if($arUser = $rsUser->Fetch())
		{
			return $arUser;
		}
		return false;
	}
	public  function addPartner($userBitrix=null)
	{
		if($userBitrix===null) return false;
		$check = $this->CheckUSER($userBitrix['ID'], 'N');
		if ($check == -1)
		{
			return $userBD = $this->AddUSER($userBitrix, 'N');
		}
		else if($check > 0)
		{
			return $userBD = $this->UpdateUSER($userBitrix, 'N');
		}
		return false;
	}
	public  function getLotID($IBLOCK_ELEMENT_ID=null)
	{
		if($IBLOCK_ELEMENT_ID===null) return false;
		$res = CIBlockElement::GetProperty(
				self::$IBLOCKID, 
				$IBLOCK_ELEMENT_ID,
				"sort", 
				"asc", 
				array("CODE" => "LOT_ID")
		);
		
		if ($ob = $res->GetNext())
		{
			return $ob['VALUE'];
		}
		return false;
	}
	public  function getTenderInfo($IBLOCK_SECTION_ID=null)
	{
		$res = CIBlockSection::GetByID($IBLOCK_SECTION_ID);
		if($tender = $res->GetNext())
		{
			return $tender;
		}
		return false;
	}
	public  function getLotInfo($lotID)
	{
		$res = CIBlockElement::GetByID($lotID);
		if($lot = $res->GetNext())
		{
			return $lot;
		}
		return false;
	}
	public  function checkTenderChange()
	{
		$activeTNDR = array();
		$arOrder = Array("SORT"=>"ASC");
		$arFilter = Array('ACTIVE'=> 'Y','IBLOCK_ID'=>self::$IBLOCKID);
		$bIncCnt = false;
		$Select = Array();
		$rsSect = CIBlockSection::GetList($arOrder,$arFilter,$bIncCnt,$Select,$NavStartParams);
		while($arSect = $rsSect->Fetch())
		{
			$activeTNDR[]= $arSect;
		}
		$this->ActiveTNDR = $activeTNDR;
		$this->checkBDandIblock();
		return $activeTNDR;
	}
	public  function checkLotChange()
	{
		return false;
	}
	public  function checkBDandIblock()
	{
		echo 'Тут проверяем соответствие БД и инфоблоков по наполненности<br/>';
		if(empty($this->ActiveTNDR))
		{
			return false;
		}
		foreach ($this->ActiveTNDR as $tendr)
		{
			if(empty($this->CheckTENDER($tendr['ID']))){
				$this->AddTENDER($tendr);
			}
		}
	}
	public  function __construct()
	{
		self::$DB =  new tenderBD();
		$this->bitrixModuleInclude();
		//echo 'Construct Ok!<br/>';
	}	
	public  function __destruct()
	{
		//echo 'Destruct Ok!<br/>';
	}
	private function bestRate($lotID=null)
	{
		if($lotID===null) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $bestRate = self::$DB->dbBestRate($lotID);
	}
	private function startRate($lotID=null)
	{
		if($lotID===null) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $actionID = self::$DB->dbStartRate($lotID);
	}
	private function refusedAccount($lotID = null, $UserIdBD = null)
	{
		if($UserIdBD===null || $lotID===null) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $actionID = self::$DB->dbRefusedAccount($lotID, $UserIdBD);
	}
	private function AddAccount($lotID = null, $UserIdBD = null)
	{
		if($UserIdBD===null || $lotID===null) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $actionID = self::$DB->dbAddAccount($lotID, $UserIdBD);
	}
	private function AddUSER($userBitrix=null, $insider)
	{
		if($userBitrix===null) return false;
		if($insider===null || ($insider!=='N' && $insider!=='Y') ) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $userBD = self::$DB->dbAddUSER($userBitrix, $insider);
	}
	private function UpdateUSER($userBitrix=null, $insider)
	{
		if($userBitrix===null) return false;
		if($insider===null || ($insider!=='N' && $insider!=='Y') ) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $userBD = self::$DB->dbUpdateUSER($userBitrix, $insider);
	}
	private function CheckTENDER($tenderID=null, $tenderCODE=null)
	{
		if($tenderID===null && $tenderCODE===null)
		{
			return false;
		}
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		if($tenderID!==null)
		{
			return self::$DB->dbCheckTenderByID($tenderID);
		} else {
			return self::$DB->dbCheckTenderByCODE($tenderCODE);
		}
		return false;
	}
	private function CheckLOT($lotID=null, $lotCODE=null)
	{
		if($lotID===null && $lotCODE===null) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		if($lotID!==null)
		{
			return $lotID = self::$DB->dbCheckLotByID($lotID);
		} else {
			return $lotID = self::$DB->dbCheckLotByCODE($lotCODE);
		}
		return false;
	}
	private function CheckUSER($userIdBitrix=null, $insider)
	{
		if($userIdBitrix===null) return false;
		if($insider===null || ($insider!=='N' && $insider!=='Y') ) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $userBD = self::$DB->dbCheckUSER($userIdBitrix, $insider);
	}
	private function bitrixModuleInclude()
	{
		if(!CModule::IncludeModule("iblock")){
			echo 'Модуль Инфоблоков не подключен!';
			$this->__destruct();
		}
	}
	private function freeQuery($sql=false)
	{
		if(true === ($sql === false))
		{
			return false;
		}
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return self::$DB->freeQuery($sql);
	}
	private function theComment(){
	/* *****************************************************************

	private function AddLOT($tenderID=null, $lot=null)
	{
		if($tenderID===null && $lot===null)
		{
			return false;
		}
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $lotID = self::$DB->AddLOT($tenderID, $lot);
	}
	
	private function AddTENDER($tender=null)
	{
		if($tender===null) return false;
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $tenderID = self::$DB->AddTENDER($tender);
	}

	
	private function UpdateLOT($tenderID=null, $lot=null)
	{
		if($tenderID===null && $lot===null)
		{
			return false;
		}
		if(true === (self::$DB === false))
		{
			echo 'Нет соединения с БД!';
			return false;
		}
		return $lotID = self::$DB->UpdateLOT($tenderID, $lot);
	}
	
	public  function addNewLot($lotID = null)
	{
		if($lotID === null){echo'ID empty!'; return false;}
	
		$lot = $this->getLotInfo($lotID);	// получаем информацию по Лоту от портала;
		$CheckLot = $this->CheckLOT( $lot['ID'], $lot['CODE']);	// проверяем наличие в базе лотов с данным символьным кодом
		if ($CheckLot == -1)
		{
			$tender = $this->getTenderInfo($lot['IBLOCK_SECTION_ID']); // определяем какому тендеру принадлежит Лот на портале;
			$tenderBD = $this->CheckTENDER($tender['ID'], $tender['CODE']); //проверяем наличие такого тендара в базе;
			if($tenderBD == -1)
			{
				$tenderBD = $this->AddTENDER($tender);
				return $lotID = $this->AddLOT($tenderBD, $lot);
			} else if ($tenderBD > 0)
			{
				return $lotID = $this->AddLOT($tenderBD, $lot);
			}
		}
		else if($CheckLot > 0)
		{
			$tender = $this->getTenderInfo($lot['IBLOCK_SECTION_ID']);
			$tenderBD = $this->CheckTENDER($tender['ID'], $tender['CODE']);
			if($tenderBD == -1)
			{
				$tenderBD = $this->AddTENDER($tender);
				return $lotID = $this->UpdateLOT($tenderBD, $lot);
			} else if ($tenderBD > 0)
			{
				return $lotID = $this->UpdateLOT($tenderBD, $lot);
			}
		}
		return false;
	}
	
	************************************************************* */
	}
}
