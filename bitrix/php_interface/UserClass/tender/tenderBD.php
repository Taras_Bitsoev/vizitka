<?
class tenderBD {
	private static $dbhost = '192.168.1.14';		// Имя хоста БД
	private static $dbusername = 'bitrix';			// Пользователь БД root
	private static $dbpass = '123qweasd';			// Пароль к базе   123qweasd 
	private static $dbname = 'report_ms_portal';	// Имя базы
	private static $link = '1';						// Соединение с БД


	
	public function dbBestRate($lotID, $agr = 'MIN') //$agr = MIN / MAX
	{ 
		if($lotID===null && ($agr !== 'MIN' && $agr !== 'MAX')) return false;
		
		$sql  = " SELECT $agr(RATE) FROM tndr_auction ";
		$sql .= " WHERE ID_LOT='$lotID' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult[$agr.'(RATE)'];
		}
		return '-1';
	}	
	public function dbStartRate($lotID=null)
	{
		if($lotID===null) return false;
		$sql  = "SELECT START_RATE FROM tndr_lot ";
		$sql .= "WHERE ID='$lotID' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['START_RATE'];
		}
		return '-1';
		
	}
	public function dbCheckAccount($lotID=null, $UserIdBD = null)
	{
		if($UserIdBD===null || $lotID===null) return false;
		
		$sql  = "SELECT ID FROM tndr_accounts ";
		$sql .= "WHERE ID_LOT='$lotID' AND ID_USER='$UserIdBD' AND REFUSED='N' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbRefusedAccount($lotID = null, $UserIdBD = null)
	{
		if($UserIdBD===null || $lotID===null) return false;
		$check = $this->dbCheckAccount($lotID, $UserIdBD);
		if($check > 0)
		{
			$sql  = " UPDATE `tndr_accounts` SET `REFUSED`='Y' ";
			$sql .=	" WHERE ID_LOT='$lotID' AND ID_USER='$UserIdBD' AND REFUSED='N'";
			$query =  mysqli_query(self::$link,($sql));
			if ( !$query || mysqli_error() )
			{
				echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
				echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
				exit;
			}
			return $proveID = $this->dbCheckAccount($lotID, $UserIdBD);
		} else {
			return $check;
		}
		return false;
	}
	public function dbAddAccount($lotID = null, $UserIdBD = null)
	{
		if($UserIdBD===null || $lotID===null) return false;
		$check = $this->dbCheckAccount($lotID, $UserIdBD);
		if($check == -1)
		{
			$sql  = "INSERT INTO `tndr_accounts` (`ID`, `ID_LOT`, `ID_USER`, `REFUSED`)"; // `DATE_ADD`,
			$sql .=	"VALUES ( '', '$lotID', '$UserIdBD', 'N') ";
			$query =  mysqli_query(self::$link,($sql));
			if ( !$query || mysqli_error() )
			{
				echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
				echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
				exit;
			}
			return $proveID = $this->dbCheckAccount($lotID, $UserIdBD);
		} else {
			return $check;
		}
		return false;
	}
	public function dbUpdateUSER($userBitrix=null, $insider) //$insider 'Y' or 'N'
	{
		if($userBitrix===null) return false;
		if($insider===null || ($insider!=='N' && $insider!=='Y') ) return false;
		
		if($insider === 'N')
		{
			$person = "N";
			$id_departament = "";
			$ltdID = $this->dbgetLtdID($userBitrix);
		} else {
			$person = "Y";
			$id_departament = $userBitrix['UF_DEPARTMENT']['0'];
			$ltdID = "";
		}
		$UserBD = $this->dbCheckUSER($userBitrix['ID'], $insider);
		$ltdNew = $this->dbUpdateLTD($ltdID, $UserBD, $userBitrix);
		$sql  = " UPDATE `tndr_user` ";
		$sql .= " SET `FIRST_NAME`= '$userBitrix[LAST_NAME]', `NAME`='$userBitrix[NAME]', ";
		$sql .= " `SECOND_NAME`='$userBitrix[SECOND_NAME]', ";
		$sql .= " `PERSON`='$person', `LTD`='$ltdNew', `INSIDER`='$insider', `ID_DEPARTAMENT`='$id_departament' ";
		$sql .= " WHERE `ID_BITRIX_USER` = '$userBitrix[ID]'";
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		return $userBD = $this->dbCheckUSER($userBitrix['ID'], $insider);
	}
	public function dbAddUSER($userBitrix=null, $insider) //$insider 'Y' or 'N'
	{
		if($userBitrix===null) return false;
		if($insider===null || ($insider!=='N' && $insider!=='Y') ) return false;
		if($insider === 'N')
		{
			$person = "N";
			$id_departament = "";
			$ltdID = $this->dbgetLtdID($userBitrix);
			if(empty($ltdID))
			{
				echo 'Нет идентификатора партнерской организации<br>';
				return false;
			}
		} else {
			$person = "Y";
			$id_departament = $userBitrix['UF_DEPARTMENT']['0'];
			if(empty($id_departament))
			{
				echo 'Нет идентификатора департамента<br>';
				return false;
			}
			$ltdID = "";
		}
		
		$sql  = "INSERT INTO `tndr_user` (`ID`, `FIRST_NAME`, `NAME`, `SECOND_NAME`, ";
		$sql .= " `PERSON`, `LTD`, `INSIDER`, `ID_DEPARTAMENT`, `ID_BITRIX_USER`)";
		$sql .=	"VALUES ( '', '$userBitrix[LAST_NAME]', '$userBitrix[NAME]', '$userBitrix[SECOND_NAME]', ";
		$sql .=	" '$person', '$ltdID', '$insider', '$id_departament', '$userBitrix[ID]' ) ";
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		$userBD = $this->dbCheckUSER($userBitrix['ID'], $insider);
		$this->dbUpdateLTD($ltdID, $userBD);
		return $userBD;
	}
	public function dbCheckUSER($userIdBitrix=null, $insider) //$insider 'Y' or 'N'
	{
		if($userIdBitrix===null) return false;
		if($insider===null || ($insider!=='N' && $insider!=='Y') ) return false;;
		$sql = "SELECT ID FROM tndr_user WHERE ID_BITRIX_USER='$userIdBitrix' AND INSIDER = '$insider' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckLotByCODE($lotCODE=null)
	{
		if($lotCODE===null) return false;
	
		$sql = "SELECT ID FROM tndr_lot WHERE CODE_BITRIX='$lotCODE' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckLotByID($lotID=null)
	{
		if($lotID===null) return false;
	
		$sql = "SELECT ID FROM tndr_lot WHERE ID_BITRIX='$lotID' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckTenderByCODE($tenderCODE=null)
	{
		if($tenderCODE===null) return false;
		$sql = "SELECT ID FROM tndr_tender WHERE CODE_BITRIX='$tenderCODE' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function dbCheckTenderByID($tenderID=null)
	{
		if($tenderID===null) return false;
		$sql = "SELECT ID FROM tndr_tender WHERE ID_BITRIX='$tenderID' ";
		$query =  mysqli_query(self::$link,($sql));
		if (!$query && mysqli_error())
		{
			echo "Ошибка базы, не удалось получить результат запроса\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	public function freeQuery($sql)
	{
		$sql = $sql;
		$query =  mysqli_query(self::$link,($sql));
		if (!$query)
		{
			echo "Ошибка базы, не удалось получить список таблиц\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
	
		while ($arResult = mysqli_fetch_assoc($query))
		{
			$Result[] = $arResult;
		}

		return $Result;
	}
	public function Fdbname ()
	{
		return self::$dbname;
	}
	public function dbnameconnect ()
	{
		return self::$link;
	}	
	public function format_to_save_string ($text)
	{
		$text = addslashes($text);
		$text= htmlspecialchars ($text);
		$text = addslashes($text);
		$username = preg_replace("/[^А-Яа-яA-Za-z0-9]/i", "", $_GET['username']);
		$text = str_replace("'/\|",'',$text);
		$text = str_replace("\r\n",' ',$text);
		$text = str_replace("\n",' ',$text);
		return $text;
	}
	public function convert_time_to_sql($time) {
		if(!isset($time)) return;
		//in  DD.MM.YYYY HH:MM:SS
		$two_side = explode(' ',$time);
		$left_side = explode('.',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	public function convert_sql_to_time($sqltime) {
		//in  DD.MM.YYYY HH:MM:SS
		if(!isset($sqltime)) return;
		$two_side = explode(' ', $sqltime);
		$left_side = explode('-',$two_side['0']); //  DD.MM.YYYY
		//out YYYY-MM-DD HH:MM:SS
		return $left_side['2'].'-'.$left_side['1'].'-'.$left_side['0'].' '.$two_side['1'];
	}
	public function take_now_time_MONTH () {
		//MONTH,
		$format = 'm';
		$NOW_MONTH = date($format,time());
		return $NOW_MONTH;
	}
	public function take_now_time_YEAR () {
		//YEAR,
		$format = 'Y';
		$NOW_YEAR = date($format,time());
		return $NOW_YEAR;
	}
	public function __construct()
	{
		//echo 'Устанавливаем соедениение с базой данных!<br>';
		self::$link = mysqli_connect(self::$dbhost, self::$dbusername, self::$dbpass, self::$dbname);
		if (!self::$link) {
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		if (!self::$link->set_charset("utf8"))
		{
			printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
		}
	}
	public function __destruct() {
		if(!mysqli_close(self::$link))
		{
			echo 'Connect with DB NOT close!<br>';
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		} 
		//echo 'Соедениение с базой данных закрыто!<br>';
	}
	public function dbgetLtdINFO ($ltdID=null)
	{
		if($ltdID===null) return false;
	
		$sql = "SELECT * FROM tndr_ltd WHERE ID='$ltdID'";
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult;
		}
		return false;
	}
	public function dbgetLtdID($userBitrix=null)
	{
		if($userBitrix===null) return false;
		$Check = $this->dbCheckLTD($userBitrix['ID']);
		if($Check == -1)
		{
			return $ltdID = $this->dbAddLTD($userBitrix);
		} else {
			return $Check;
		}
	}
	public function dbUpdateLTD($ltdID=null, $userBD=null, $userBitrix=null)
	{
		if($ltdID===null || $userBD=== null) return false;
		$UserBDSET = " `USER_ID`= '$userBD'";
		if($userBitrix !== null)
		{
			$ZIP     = $userBitrix['WORK_ZIP'];
			$COUNTRY = $userBitrix['WORK_COUNTRY'];
			$CITY    = $userBitrix['WORK_CITY'];
			$STREET  = $userBitrix['WORK_STREET'];
			$PHONE   = $userBitrix['WORK_PHONE'];
			$COMPANY = $userBitrix['WORK_COMPANY'];
	
			$UserBDSET .= " ,`ZIP`='$ZIP',`COUNTRY`='$COUNTRY',`CITY`='$CITY',";
			$UserBDSET .= " `STREET`='$STREET',`PHONE`='$PHONE', `LTD_NAME`='$COMPANY' ";
		}
	
		$sql  = " UPDATE `tndr_ltd` ";
		$sql .= " SET $UserBDSET ";
		$sql .= " WHERE `ID` = '$ltdID' ";

		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		return $ltdID = $this->dbCheckBDLTD($userBD);
	}
	public function dbAddLTD($userBitrix=null)
	{
		if($userBitrix===null) return false;
		
		$ZIP     = $userBitrix['WORK_ZIP'];
		$COUNTRY = $userBitrix['WORK_COUNTRY'];
		$CITY    = $userBitrix['WORK_CITY'];
		$STREET  = $userBitrix['WORK_STREET'];
		$PHONE   = $userBitrix['WORK_PHONE'];
		$COMPANY = $userBitrix['WORK_COMPANY'];
	
		$sql  = " INSERT INTO `tndr_ltd`( `ID`, `ZIP`, `COUNTRY`, `CITY`, `STREET`, `PHONE`, ";
		$sql .= " `USER_ID`, `ID_BITRIX_USER`, `LTD_NAME` ) ";
		$sql .= " VALUES ( '', '$ZIP', '$COUNTRY', '$CITY', '$STREET', '$PHONE', ";
		$sql .= " '', '$userBitrix[ID]', '$COMPANY') ";
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		return $ltdID = $this->dbCheckLTD($userBitrix['ID']);
	}
	public function dbCheckLTD($userBitrixID=null)
	{
		if($userBitrixID===null) return false;
	
		$sql = "SELECT ID FROM tndr_ltd WHERE ID_BITRIX_USER='$userBitrixID'";
	
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
	private function dbCheckBDLTD($userBD=null)
	{
		if($userBD===null) return false;
	
		$sql = "SELECT ID FROM tndr_ltd WHERE USER_ID='$userBD'";
	
		$query =  mysqli_query(self::$link,($sql));
		if ( !$query || mysqli_error() )
		{
			echo "Ошибка базы, не удалось выполнить запрос\n! <br>";
			echo 'Ошибка MySQL: ' . mysqli_error().'<br>';
			exit;
		}
		while ($arResult = mysqli_fetch_assoc($query))
		{
			return $arResult['ID'];
		}
		return '-1';
	}
}