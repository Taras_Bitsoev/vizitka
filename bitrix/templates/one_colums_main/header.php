<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="google-site-verification" content="WpDwSqCc3G_Dr0B-OGC97M5dQ9B_ckxx1TZN6op9wT4" />
<meta name="keywords" content="аптеки, аптека, мед-сервис, аптеки мед-сервис, компания мед-сервис"/>
<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/common.css" />
<?$APPLICATION->ShowHead();?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/colors.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/wwflakes-webfont.css" />
<title><?$APPLICATION->ShowTitle()?></title>
<?/* 
<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAosN5daBWjFKLdUUsui0yJn5qxe0GQU5A&sensor=FALSE">
</script> */?>
<? /*
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
   
<script src="<?=SITE_TEMPLATE_PATH?>/js/gmaps.js" type="text/javascript"></script>
*/?>
<!-- jquery libs 1.9.0 -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<?/* <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/snowstorm.js"></script>*/ ?>
<!-- Add maphilight plugin -->
<script  src="<?=SITE_TEMPLATE_PATH?>/js/jquery.maphilight.min.js"  type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		$('.map').maphilight();
	});
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter30531667 = new Ya.Metrika({id:30531667,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/30531667" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body >  <? // onload="initialize()"   ?>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div id="page-wrapper">
	<div id="header">
		<div class="header-line">
			 	<div class="header-line-darck"></div>
		</div>
		<div style="clear:both;"></div>
		<div class="header-logo">
			<div class="logo-block">
				<a href="/" class="header-link">
					<span class="logo-img"></span>
				</a>
			</div>	
		</div>
		<div class="header-text">
			<h1><span>НАЦИОНАЛЬНАЯ СЕТЬ АПТЕК</span></h1>
			<span class="text-info">
				<span class="chisla">
					<?
					CModule::IncludeModule('iblock'); 
					$arFilter = array( 
					    'IBLOCK_ID' => '11',	 // ID инфоблока 
						'ACTIVE' => 'Y'		// активность элемента 
					); 
					$res = CIBlockElement::GetList(
					false,
					 $arFilter, array('IBLOCK_ID')); 
					if ($el = $res->Fetch()) 
					    echo ''.$el['CNT'];
					?>
					
					<?
					// вывел количество подразделов информационного блока "аптеки"
					/*
					 CModule::IncludeModule("iblock");
					   { 
						   $arFilter = Array(
						  "IBLOCK_ID"=> 11,
						  'ACTIVE' => 'Y'
						  
						   );
						   echo CIBlockSection::GetCount($arFilter);
					   } */
				   ?>
				   
				   	<?/*
				   		//
				   		CModule::IncludeModule("iblock");
					   { 
						   /* $arFilter = Array(
						  'BLOCK_ID'=> '11',
						  'ACTIVE' => 'Y',
						   );
						    $select  = Array(
						    'NAME' => 'zakarpatskaya'
						    );*/
					   	/*
						   $arOrder = Array("SORT"=>"ASC");
						    $arFilter = Array(
						    	"IBLOCK_ID"=> 11,
						    	'ACTIVE' => 'Y',
						    	'ID' => '193',
						    	);
						    $Select = Array(
						    	'ID' => '218'
						    		
						    	);
						    $NavStartParams = false;
						    
						   echo CIBlockSection::GetCount($Select);
					   }*/
				   ?>
				  
				</span>
				<span class="chisla-text">аптек</span>
				<span class="chisla">111</span>
				<span class="chisla-text">городов</span>
			</span>
		</div>
		<div class="header-position">
		<div class="header-hotline">
			<div class="header-position-hotline">
				<span class="hotline-text">Горячая линия</span><br/>
				<span class="hotline-telefon">0-800-50-52-53</span><br/>
				<div class="hotline-button mainfedback1">				
					<div class="button-preloader"></div>
					<div class="button-text">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</div>			
				</div>
			</div>
			<div class="overlay1"></div>				
			<div class="popup1">	
				<div class="mfeedback">
						<form method="POST" action="<?=SITE_TEMPLATE_PATH?>/ajax_tel.php">
							<div class="mf-name">
								<div class="mf-text">Ваше имя<span class="mf-req">*</span></div>
								<input type="text" pattern="^[А-Яа-яЁё\s]+$" required value="" name="user_name" placeholder="Иван Иванов">
							</div>
							<div class="mf-email">
							<div class="mf-text">
								<b style="padding-left:10px;">Ваш телефон</b><span class="mf-req">*</span>
							</div>	
								<input type="text" pattern="[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}" placeholder="000-000-00-00" required value="" name="user_phone">
							</div>
								<div class="mf-message">
									<div class="mf-text">Сообщение<span class="mf-req">*</span></div>
									<textarea pattern="^[А-Яа-яЁё\s]+$" required name="MESSAGE"></textarea>
								</div>
							<input type="submit" value="Отправить" name="submit" class="send">
						</form>
				</div>
				<div class="close_window1">x</div>		
					<?/*$APPLICATION->IncludeComponent(
							"bitrix:main.feedback", 
								"template_rubin_phone", 
							Array(
							
							),
							false
					);*/?>
			</div>	
			<script type="text/javascript">
					$(document).ready(function(){
					$('.popup1 .close_window1, .overlay1').click(function (){
					$('.popup1, .overlay1').css('opacity','0');
					$('.popup1, .overlay1').css('visibility','hidden');
					});
					$('.mainfedback1').click(function (e){
					$('.popup1, .overlay1').css('opacity','1');
					$('.popup1, .overlay1').css('visibility','visible');
					e.preventDefault();
					});
					});
			</script>	
		</div>
		<div class  ="header-support">
			<span class="support-text">Уголок клиента</span><br/>
			<span class="support-block-text">Здесь вы можете оставить свой отзыв<br/>или пожелания</span><br/>
			<div class="support-button mainfedback" style="cursor: pointer;">				
				<div class="support-button-text">ОСТАВИТЬ ОТЗЫВ</div>			
			</div>	
				
			<div class="overlay"></div>				
			<div class="popup">	
				<div class="close_window">x</div>			
					<?$APPLICATION->IncludeComponent(
							"bitrix:main.feedback", 
								"template_rubin", 
							Array(
								"USE_CAPTCHA" => "Y",
								"OK_TEXT" => "Спасибо, ваше сообщение принято.",
								"EMAIL_TO" => "m.bilitskaya@med-service.dp.ua",
								"REQUIRED_FIELDS" => array(
								),
								"EVENT_MESSAGE_ID" => array(
								)
							),
							false
					);?>
			</div>
			
			<script type="text/javascript">
					$(document).ready(function(){
					$('.popup .close_window, .overlay').click(function (){
					$('.popup, .overlay').css('opacity','0');
					$('.popup, .overlay').css('visibility','hidden');
					});
					$('.mainfedback').click(function (e){
					$('.popup, .overlay').css('opacity','1');
					$('.popup, .overlay').css('visibility','visible');
					e.preventDefault();
					});
					});
			</script>	
		</div>		
		</div>
		</div>
		<div style="clear:both;"></div>
	<? // </div>  ?>
	<div id="main-menu">
		<div class="wrap-ullist">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multilevel", array(
				"ROOT_MENU_TYPE" => "top",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "N",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "4",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "Y",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
		</div>
	<?$APPLICATION->IncludeComponent("bitrix:search.form", "template1", array(
	"PAGE" => "#SITE_DIR#search/index.php",
	"USE_SUGGEST" => "N"
	),
	false
	);?>
	</div>
<?/*  	
	<div class="NY">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/snowbg.PNG" alt="" />
	</div>
	<style>
		.NY img{
			width: 100%;
			
		}
	</style>
*/?>
	<div id="page-body">
